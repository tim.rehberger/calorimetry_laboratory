import json

from typing import Dict, List, Tuple

import h5py as h5
import matplotlib.pyplot as plt
import numpy as np

from statistics import mean
from statistics import pstdev


def plot_temp_over_time(
    data: List[np.ndarray],
    time: List[np.ndarray],
    legend: List[str],
    x_label: str,
    y_label: str,
) -> None:
    """Plots temperature data over time with error bars for multiple datasets.

    This function creates a plot representing temperature data against time
    for multiple sensors or datasets. Each dataset's standard deviation is visualized
    with error bars.

     Args:
        data (List[np.ndarray]): A list of numpy arrays where each array represents
                                 the temperature data (with standard deviation).
                                 Each array should have a shape of (2, n), with n
                                 representing the number of data points.
        time (List[np.ndarray]): A list of numpy arrays with the time data corresponding
                                 to each dataset in `data`.
        legend (List[str]): A list of strings that label each dataset in the legend.
        x_label (str): The label for the x-axis (time).
        y_label (str): The label for the y-axis (temperature).

    """
    # init the matplotlib.axes.Axes and matplotlib.figure.Figure Object for later plot
    fig, ax = plt.subplots(1, 1)
    markers = ["o", "^", "2", "p", "D"]
    
    for i in range(len(data)):
        # TODO: draw a plot using the ax.errorbar(...) function
        # Document: https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.errorbar.html
        
        ax.errorbar(time[i], data[i][0, :], data[i][1, :], label = legend[i])
        # DONE #

    # Errorbars removed from Legend
    legend_handles, labels = ax.get_legend_handles_labels()
    legend_handles = [h[0] for h in legend_handles]

    # TODO: set legend, x- and y- axis label.
    
    ax.set_ylabel(y_label)
    ax.set_xlabel(x_label)
    ax.legend()

    # DONE #

    ax.ticklabel_format(scilimits=(0, 3))


def get_plot_data_from_dataset(
    data_path: str, group_path: str
) -> Dict[str, np.ndarray]:
    """Get the necessary data from the dataset to plot.

    This function returns the data in a HDF5 file in all subgroups of a group in 'group_path'
    and automatically categorizes and names the data based on the name of the dataset as well as the metadata.

    Args:
        data_path (str): path to HDF5 file.
        group_path (str): path in HDF5 to group.

    Returns:
        Dict[str, np.ndarray]: Data for plot in a dict.

    Example:
        Output (example data):
        {
            "temperature": np.array([
                            [24.89, 24.92, 24.00, 25.39],
                            [24.89, 24.92, 24.00, 25.39],
                            [24.89, 24.92, 24.00, 25.39]
                        ]) -> temperature from each sensor, The first dimension(row) represents the sensor.
            "timestamp": np.array([
                            [0.43, 1.60, 3.05, 4.25],
                            [0.81, 2.13, 3.49, 4.62],
                            [1.34, 2.60, 3.85, 5.08],
                        ]) -> timestamp for each sensor, The first dimension(row) represents the sensor.
            "name": np.array(["sensor_1", "sensor_2", "sensor_3"]) -> name of each sensor should be hier
        }

    """
    temperature = []
    timestamp = []
    name = []

    with h5.File(data_path) as data:
        group = data[group_path]
        subgroups = []
        min_len = None
        start_time = None

        for subgroup in group:
            try:
                dataset_start_time = group[subgroup]["timestamp"][0]
                dataset_len = len(group[subgroup]["timestamp"])

                # Find the minimum length of the data set.
                if min_len is None:
                    min_len = dataset_len
                elif dataset_len < min_len:
                    min_len = dataset_len

                subgroups.append(subgroup)

                # Only group with dataset called timestamp will be read.
            except KeyError:
                continue

            # TODO: Find the start time point of the measurement.
            
            if start_time is None:
                start_time = dataset_start_time
            elif dataset_start_time < start_time:
                start_time = dataset_start_time

            # DONE #
        
        temperature = np.zeros((len(subgroups), min_len))
        timestamp = np.zeros((len(subgroups), min_len))
        namelist = []
        
        for i, subgroup in enumerate(subgroups):
            # TODO: Save data in to the lists temperature, time and name.#
            
            j = 0
            while j < min_len:
                temperature[i,j] = group[subgroup]["temperature"][j]
                timestamp[i,j] = group[subgroup]["timestamp"][j]
                j += 1
                
            namelist.append(group[subgroup].attrs["name"])
            
        name = np.array(namelist)
            
            # Data for each sensor must have the same length because of np.ndarray will be use in the output.

            # DONE #

    # TODO: return the output dict.
    
    diction = {
              "temperature": temperature, 
              "timestamp": timestamp,
              "name": name
              }

    return diction

    # DONE #


def cal_mean_and_standard_deviation(data: np.ndarray) -> np.ndarray:
    """Calculating mean and standard deviation for raw data of multiple sensors.

    Args:
        data (np.ndarray): raw data in a 2 dimensional array (m, n), the first dimension should not be 1 if
                           there are multiple measurements at the same time (and place).

    Returns:
        np.ndarray: mean of raw data with standard deviation in a 2D ndarray with shape (2, n).

    """
    # TODO: Calculate the mean and standard deviation of the first dimension and return the result as a
    # two-dimensional (2, n)-shaped ndarray.
    
    dimen = data.shape
    n = dimen[1]
    
    if dimen[0] == 1:
        
        values = np.zeros((2, n))
        
        values[0, :] = mean(data[0, :])
        values[1, :] = pstdev(data[0, :], values[0, 0])
        
    else:
        
        values = np.zeros((2, n))
        i = 0
    
        while i < n:
        
            values[0, i] = mean(data[:,i])
            values[1, i] = pstdev(data[:,i], values[0, i])
            
            i += 1
        
            
    return values


    # DONE #


def get_start_end_temperature(
    temperature_data: np.ndarray, threshold: float = 0.05
) -> Tuple[float, float]:
    """Calculates the high and low temperatures from a dataset.

    This function computes the (average of) the highest temperatures and the (average of) the lowest temperatures
    within a given threshold from the maximum and minimum temperatures recorded in the dataset. These are
    considered as the ending and starting temperatures respectively.

    Args:
        temperature_data (np.ndarray): The temperature dataset as a 2D numpy array.
        threshold (float): The threshold percentage used to identify temperatures close to the maximum
                           and minimum values as high and low temperatures respectively. Default to 0.05

    Returns:
        Tuple[float, float]: A tuple containing the average high temperature first and the average low
                             temperature second.

    """
    # TODO: You don't have to implement this function exactly as docstring expresses it, it just gives
    # an idea that you can refer to. The goal of this function is to obtain from the data the high and
    # low temperatures necessary to calculate the heat capacity.

    values = [] # values that are not rising
    index = [] # index of each value where the temperature is still rising (useful to calculate the heating time for the calorimeter)
    
    j = len(temperature_data[0,:])
    
    j -= 1
    
    # Loop will check for every value if the next 2 values are rising.
    # If the next 2 values are not rising then the current value will be written in a new list
    
    
    # Expanding List by 2 so the loop also works for the last 2 temperature values
    temperature_data_new = np.append(temperature_data[0,:], [temperature_data[0,j-2], temperature_data[0,j-1]])
    
    i = 0
    
    while i < j:
        
        if temperature_data[0,i] >= temperature_data[0,i+1]:
            
            values.append(temperature_data[0,i])
            
        elif temperature_data[0,i+1] >= temperature_data[0,i+2]:
            
            values.append(temperature_data[0,i])
            
        else:
            
            index.append(i)
            
        i += 1
        
    
    minlist = []
    maxlist = []
    values_M = mean(values)
    
    for v in values:
        
        if v <= values_M:
            
            minlist.append(v)
            
        else:
            
            maxlist.append(v)
            
    endvalue = mean(maxlist)
    startvalue = mean(minlist)
            
    return endvalue, startvalue, index

    # DONE #
